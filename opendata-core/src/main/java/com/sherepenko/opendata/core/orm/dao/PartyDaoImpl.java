package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.sherepenko.opendata.core.data.PartyItem;

import java.sql.SQLException;

public class PartyDaoImpl extends BaseDaoImpl<PartyItem, Integer> implements PartyDao {

    public PartyDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, PartyItem.class);
        setObjectCache(ReferenceObjectCache.makeSoftCache());
    }
}
