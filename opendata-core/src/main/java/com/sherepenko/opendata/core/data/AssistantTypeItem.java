package com.sherepenko.opendata.core.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.sherepenko.opendata.core.json.deserialization.AssistantTypeItemDeserializer;
import com.sherepenko.opendata.core.orm.dao.AssistantTypeDaoImpl;

@DatabaseTable(tableName = "assistant_types", daoClass = AssistantTypeDaoImpl.class)
@JsonDeserialize(using = AssistantTypeItemDeserializer.class)
public class AssistantTypeItem extends BaseDaoEnabled {

    @DatabaseField(columnName = "id", id = true)
    protected int mId;
    @DatabaseField(columnName = "type", dataType = DataType.LONG_STRING)
    protected String mType;

    AssistantTypeItem() {
    }

    @JsonCreator
    public AssistantTypeItem(@JsonProperty("id") final int id,
                             @JsonProperty("type") final String type) {
        mId = id;
        mType = type;
    }

    public int getId() {
        return mId;
    }

    public String getType() {
        return mType;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (other == null) {
            return false;
        } else if (getClass() != other.getClass()) {
            return false;
        }

        AssistantTypeItem item = ((AssistantTypeItem) other);
        return mId == item.mId;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + mId;
        result = 31 * result + (mType != null ? mType.hashCode() : 0);
        return result;
    }
}
