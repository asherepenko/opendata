package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.sherepenko.opendata.core.data.AssistantItem;

import java.sql.SQLException;

public class AssistantDaoImpl extends BaseDaoImpl<AssistantItem, Integer> implements AssistantDao {

    public AssistantDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, AssistantItem.class);
        setObjectCache(ReferenceObjectCache.makeSoftCache());
    }
}
