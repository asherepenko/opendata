package com.sherepenko.opendata.core.loaders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.sherepenko.opendata.core.data.AssistantTypeItem;
import com.sherepenko.opendata.core.data.PartyItem;
import com.sherepenko.opendata.core.data.RegionItem;
import com.sherepenko.opendata.core.utils.ResourceUtils;

import java.io.IOException;
import java.util.List;

public class InitialLoader {

    public static void setup() {
        try {
            List<AssistantTypeItem> assistantTypes = getAssistantTypes("json/assistant_types.json");
            List<PartyItem> parties = getParties("json/parties.json");
            List<RegionItem> regions = getRegions("json/regions.json");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<AssistantTypeItem> getAssistantTypes(final String path) throws IOException {
        return new ObjectMapper().readValue(ResourceUtils.getResource(path),
                TypeFactory.defaultInstance()
                        .constructCollectionType(List.class, AssistantTypeItem.class));
    }

    private static List<PartyItem> getParties(final String path) throws IOException {
        return new ObjectMapper().readValue(ResourceUtils.getResource(path),
                TypeFactory.defaultInstance()
                        .constructCollectionType(List.class, PartyItem.class));
    }

    private static List<RegionItem> getRegions(final String path) throws IOException {
        return new ObjectMapper().readValue(ResourceUtils.getResource(path),
                TypeFactory.defaultInstance()
                        .constructCollectionType(List.class, RegionItem.class));
    }
}
