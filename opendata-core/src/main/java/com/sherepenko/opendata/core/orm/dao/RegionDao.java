package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.Dao;
import com.sherepenko.opendata.core.data.RegionItem;

public interface RegionDao extends Dao<RegionItem, Integer> {
}
