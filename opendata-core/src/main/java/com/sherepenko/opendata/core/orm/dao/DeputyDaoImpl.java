package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.sherepenko.opendata.core.data.DeputyItem;

import java.sql.SQLException;

public class DeputyDaoImpl extends BaseDaoImpl<DeputyItem, Integer> implements DeputyDao {

    public DeputyDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DeputyItem.class);
        setObjectCache(ReferenceObjectCache.makeSoftCache());
    }
}
