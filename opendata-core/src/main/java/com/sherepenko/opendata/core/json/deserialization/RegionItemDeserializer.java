package com.sherepenko.opendata.core.json.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.sherepenko.opendata.core.data.RegionItem;
import com.sherepenko.opendata.core.orm.OrmHelper;

import java.io.IOException;

public class RegionItemDeserializer extends JsonDeserializer<RegionItem> {

    @Override
    public RegionItem deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        final JsonNode jsonRoot = jsonParser.getCodec().readTree(jsonParser);
        final RegionItem region = new RegionItem(
                jsonRoot.get("id").asInt(),
                jsonRoot.get("name").asText());
        OrmHelper.get().insertRegion(region);
        return region;
    }
}
