package com.sherepenko.opendata.core.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sherepenko.opendata.core.data.DeclarationItem;
import com.sherepenko.opendata.core.json.deserialization.DeclarationsResponseDeserializer;

import java.util.List;

@JsonDeserialize(using = DeclarationsResponseDeserializer.class)
public class DeclarationsResponse extends ServiceResponse<DeclarationItem> {

    public DeclarationsResponse(final List<DeclarationItem> items) {
        super(items);
    }
}
