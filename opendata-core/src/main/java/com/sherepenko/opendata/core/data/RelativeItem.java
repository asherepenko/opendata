package com.sherepenko.opendata.core.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.sherepenko.opendata.core.orm.dao.RegionDaoImpl;
import com.sherepenko.opendata.core.orm.dao.RelativeDaoImpl;
import com.sun.org.apache.regexp.internal.RE;
import org.h2.util.StringUtils;

import java.util.StringTokenizer;

@DatabaseTable(tableName = "relatives", daoClass = RelativeDaoImpl.class)
public class RelativeItem extends PersonItem {

    @DatabaseField(columnName = "id", generatedId = true)
    protected int mId;
    @DatabaseField(columnName = "mp_id", foreign = true, foreignAutoRefresh = true)
    protected DeputyItem mDeputy;
    @DatabaseField(columnName = "declaration_id", foreign = true, foreignAutoRefresh = true)
    protected DeclarationItem mDeclaration;
    @DatabaseField(columnName = "relationship")
    protected String mRelationship;

    public int getId() {
        return mId;
    }

    public DeputyItem getDeputy() {
        return mDeputy;
    }

    public void setDeputy(final DeputyItem deputy) {
        mDeputy = deputy;
    }

    public DeclarationItem getDeclaration() {
        return mDeclaration;
    }

    public void setDeclaration(final DeclarationItem declaration) {
        mDeclaration = declaration;
    }

    public String getRelationship() {
        return mRelationship;
    }

    public void setRelationship(final String relationship) {
        mRelationship = relationship;
    }
}
