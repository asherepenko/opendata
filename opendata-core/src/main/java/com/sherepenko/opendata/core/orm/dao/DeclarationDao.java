package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.Dao;
import com.sherepenko.opendata.core.data.DeclarationItem;

public interface DeclarationDao extends Dao<DeclarationItem, Integer> {
}
