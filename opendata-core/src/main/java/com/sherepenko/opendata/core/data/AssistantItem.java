package com.sherepenko.opendata.core.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.sherepenko.opendata.core.orm.dao.AssistantDaoImpl;

@DatabaseTable(tableName = "assistants", daoClass = AssistantDaoImpl.class)
public class AssistantItem extends PersonItem {

    @DatabaseField(columnName = "id", generatedId = true)
    protected int mId;
    @DatabaseField(columnName = "mp_id", foreign = true, foreignAutoRefresh = true)
    protected DeputyItem mDeputy;
    @DatabaseField(columnName = "type_id", foreign = true, foreignAutoRefresh = true)
    protected AssistantTypeItem mAssistantType;

    public int getId() {
        return mId;
    }

    public DeputyItem getDeputy() {
        return mDeputy;
    }

    public void setDeputy(final DeputyItem deputy) {
        mDeputy = deputy;
    }

    public AssistantTypeItem getAssistantType() {
        return mAssistantType;
    }

    public void setAssistantType(final AssistantTypeItem assistantType) {
        mAssistantType = assistantType;
    }
}
