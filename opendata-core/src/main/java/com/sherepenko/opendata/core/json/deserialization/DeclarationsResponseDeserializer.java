package com.sherepenko.opendata.core.json.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.sherepenko.opendata.core.data.CompanyItem;
import com.sherepenko.opendata.core.data.DeclarationItem;
import com.sherepenko.opendata.core.data.DeputyItem;
import com.sherepenko.opendata.core.data.RelativeItem;
import com.sherepenko.opendata.core.json.DeclarationsResponse;
import com.sherepenko.opendata.core.orm.OrmHelper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DeclarationsResponseDeserializer extends JsonDeserializer<DeclarationsResponse> {

    private static final String NACP_SRC = "NACP";
    private static final DateFormat sFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Override
    public DeclarationsResponse deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        final JsonNode jsonRoot = jsonParser.getCodec().readTree(jsonParser);

        List<DeclarationItem> declarations =
                StreamSupport.stream(jsonRoot.findValue("object_list").spliterator(), false)
                        .filter(this::isNacpDeclaration)
                        .map(this::createDeclarationItem)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList());
        return new DeclarationsResponse(declarations);
    }

    private boolean isNacpDeclaration(JsonNode jsonDeclaration) {
        return StringUtils.equalsIgnoreCase(jsonDeclaration.get("infocard").get("source").asText(), NACP_SRC);
    }

    private DeclarationItem createDeclarationItem(final JsonNode jsonDeclaration) {
        final DeclarationItem declaration = new DeclarationItem(jsonDeclaration.get("infocard").get("id").asText());

        parseInfoSection(declaration, jsonDeclaration);
        parseTypeSection(declaration, jsonDeclaration);
        parseFamilySection(declaration, jsonDeclaration);

        final Set<CompanyItem> companies = new HashSet<>();
        companies.addAll(parseBeneficiarySection(declaration, jsonDeclaration));
        companies.addAll(parseSharesSection(declaration, jsonDeclaration));
        companies.addAll(parseCorporateRightsSection(declaration, jsonDeclaration));
        companies.addAll(parseOrganizationsSection(declaration, jsonDeclaration));

        declaration.setCompanies(new ArrayList<>(companies));
        OrmHelper.get().insertDeclaration(declaration);
        return declaration;
    }

    private RelativeItem createRelativeItem(final DeclarationItem declaration, final JsonNode jsonRelative) {
        final RelativeItem relative = new RelativeItem();
        relative.setDeclaration(declaration);
        if (jsonRelative.get("lastname").isTextual()) {
            relative.setLastName(jsonRelative.get("lastname").asText());
        }
        if (jsonRelative.get("firstname").isTextual()) {
            relative.setFirstName(jsonRelative.get("firstname").asText());
        }
        if (jsonRelative.get("middlename").isTextual()) {
            relative.setPatronymic(jsonRelative.get("middlename").asText());
        }
        if (jsonRelative.get("subjectRelation").isTextual()) {
            relative.setRelationship(jsonRelative.get("subjectRelation").asText());
        }
        OrmHelper.get().insertRelative(relative);
        return relative;
    }

    private CompanyItem createCompanyFromBeneficiary(final DeclarationItem declaration, final JsonNode jsonCompany) {
        final CompanyItem company =
                new CompanyItem(jsonCompany.get("beneficial_owner_company_code").asText());
        company.setDeclaration(declaration);
        if (jsonCompany.get("name").isTextual()) {
            company.setName(jsonCompany.get("name").asText());
        }
        if (jsonCompany.get("legalForm").isTextual()) {
            company.setLegalForm(jsonCompany.get("legalForm").asText());
        }
        if (jsonCompany.get("address").isTextual()) {
            company.setAddress(jsonCompany.get("address").asText());
        }
        if (jsonCompany.get("phone").isTextual()) {
            company.setPhone(jsonCompany.get("phone").asText());
        }
        if (jsonCompany.get("fax").isTextual()) {
            company.setFax(jsonCompany.get("fax").asText());
        }
        return company;
    }

    private CompanyItem createCompanyFromShares(final DeclarationItem declaration, final JsonNode jsonCompany) {
        final CompanyItem company =
                new CompanyItem(jsonCompany.get("emitent_ua_company_code").asText().trim());
        company.setDeclaration(declaration);
        company.setLegalForm(company.getName().split("\\s+")[0]);
        company.setPropertyType(jsonCompany.get("typeProperty").asText(""));
        return company;
    }

    private CompanyItem createCompanyFromCorporateRights(final DeclarationItem declaration, final JsonNode jsonCompany) {
        final CompanyItem company =
                new CompanyItem(jsonCompany.get("corporate_rights_company_code").asText());
        company.setDeclaration(declaration);
        company.setName(jsonCompany.get("name").asText());
        company.setLegalForm(company.getName().split("\\s+")[0]);
        return company;
    }

    private CompanyItem createCompanyFromOrganization(final DeclarationItem declaration, final JsonNode jsonCompany) {
        final CompanyItem company = new CompanyItem(jsonCompany.get("reestrCode").asText());
        company.setDeclaration(declaration);
        company.setName(jsonCompany.get("objectName").asText());
        company.setLegalForm(jsonCompany.get("objectType").asText());
        return company;
    }

    private void parseInfoSection(final DeclarationItem declaration, final JsonNode jsonDeclaration) {
        final JsonNode jsonInfoSection = jsonDeclaration.get("infocard");

        if (jsonInfoSection != null) {
            if (jsonInfoSection.get("position").isTextual()) {
                declaration.setPosition(jsonInfoSection.get("position").asText());
            }
            if (jsonInfoSection.get("office").isTextual()) {
                declaration.setOffice(jsonInfoSection.get("office").asText());
            }
            if (jsonInfoSection.get("url").isTextual()) {
                declaration.setUrl(jsonInfoSection.get("url").asText());
            }
            if (jsonInfoSection.get("created_date").isTextual()) {
                declaration.setCreatedDate(parseDate(jsonInfoSection.get("created_date").asText()));
            }
        }
    }

    private void parseTypeSection(final DeclarationItem declaration, final JsonNode jsonDeclaration) {
        final JsonNode jsonTypeSection = jsonDeclaration.get("unified_source").get("step_0");

        if (jsonTypeSection != null && jsonTypeSection.get("declarationYear1").isInt()) {
            declaration.setYear(jsonTypeSection.get("declarationYear1").asInt());
        }
    }

    private void parseFamilySection(final DeclarationItem declaration, final JsonNode jsonDeclaration) {
        final JsonNode jsonFamilySection = jsonDeclaration.get("unified_source").get("step_2");

        if (jsonFamilySection != null) {
            declaration.setRelatives(
                    StreamSupport.stream(spliterator(jsonFamilySection.fields()), false)
                            .map(Map.Entry::getValue)
                            .map(jsonRelative -> createRelativeItem(declaration, jsonRelative))
                            .distinct()
                            .sorted()
                            .collect(Collectors.toList()));
        }
    }

    private List<CompanyItem> parseSharesSection(final DeclarationItem declaration, final JsonNode jsonDeclaration) {
        final JsonNode jsonSharesSection = jsonDeclaration.get("unified_source").get("step_7");

        if (jsonSharesSection != null) {
            return StreamSupport.stream(spliterator(jsonSharesSection.fields()), false)
                    .map(Map.Entry::getValue)
                    .map(jsonCompany -> createCompanyFromShares(declaration, jsonCompany))
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    private List<CompanyItem> parseCorporateRightsSection(final DeclarationItem declaration, final JsonNode jsonDeclaration) {
        final JsonNode jsonCorporateRightsSection = jsonDeclaration.get("unified_source").get("step_8");
        if (jsonCorporateRightsSection != null) {
            return StreamSupport.stream(spliterator(jsonCorporateRightsSection.fields()), false)
                    .map(Map.Entry::getValue)
                    .map(jsonCompany -> createCompanyFromCorporateRights(declaration, jsonCompany))
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    private List<CompanyItem> parseBeneficiarySection(final DeclarationItem declaration, final JsonNode jsonDeclaration) {
        final JsonNode jsonBeneficiarySection = jsonDeclaration.get("unified_source").get("step_9");

        if (jsonBeneficiarySection != null) {
            return StreamSupport.stream(spliterator(jsonBeneficiarySection.fields()), false)
                    .map(Map.Entry::getValue)
                    .map(jsonCompany -> createCompanyFromBeneficiary(declaration, jsonCompany))
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    private List<CompanyItem> parseOrganizationsSection(final DeclarationItem declaration, final JsonNode jsonDeclaration) {
        final JsonNode jsonOrganizationsSection = jsonDeclaration.get("unified_source").get("step_16");

        if (jsonOrganizationsSection != null) {
            return  StreamSupport.stream(spliterator(jsonOrganizationsSection.get("org").fields()), false)
                    .map(Map.Entry::getValue)
                    .map(jsonCompany -> createCompanyFromOrganization(declaration, jsonCompany))
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    private static Date parseDate(final String date) {
        try {
            return sFormatter.parse(date);
        } catch (ParseException ignore) {}

        return null;
    }

    private static <T> Spliterator<T> spliterator(final Iterator<? extends T> iterator) {
        return Spliterators.spliteratorUnknownSize(iterator, 0);
    }
}
