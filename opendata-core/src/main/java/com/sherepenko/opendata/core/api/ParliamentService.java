package com.sherepenko.opendata.core.api;

import com.sherepenko.opendata.core.json.DeputiesResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ParliamentService {

    String BASE_URL = "http://data.rada.gov.ua";

    @GET("ogd/mps/skl8/mps-data.json")
    Observable<DeputiesResponse> getDeputies();
}
