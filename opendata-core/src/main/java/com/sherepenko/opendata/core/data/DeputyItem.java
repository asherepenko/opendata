package com.sherepenko.opendata.core.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.sherepenko.opendata.core.orm.dao.DeputyDaoImpl;

import java.util.Collection;

@DatabaseTable(tableName = "deputies", daoClass = DeputyDaoImpl.class)
public class DeputyItem extends PersonItem {

    @DatabaseField(columnName = "id", id = true)
    protected int mId;
    @DatabaseField(columnName = "party_id", foreign = true, foreignAutoRefresh = true)
    protected PartyItem mParty;
    @DatabaseField(columnName = "region_id", foreign = true, foreignAutoRefresh = true)
    protected RegionItem mRegion;
    @DatabaseField(columnName = "district")
    protected int mDistrict;
    @DatabaseField(columnName = "convocation")
    protected int mConvocation;
    @DatabaseField(columnName = "thumbnail", dataType = DataType.LONG_STRING)
    protected String mThumbnail;
    @DatabaseField(columnName = "description", dataType = DataType.LONG_STRING)
    protected String mDescription;
    @DatabaseField(columnName = "emails", dataType = DataType.SERIALIZABLE)
    protected String[] mEmails;
    @DatabaseField(columnName = "phones", dataType = DataType.SERIALIZABLE)
    protected String[] mPhones;
    @DatabaseField(columnName = "socials", dataType = DataType.SERIALIZABLE)
    protected String[] mSocials;
    @ForeignCollectionField(columnName = "assistants")
    protected Collection<AssistantItem> mAssistants;
    @ForeignCollectionField(columnName = "relatives")
    protected Collection<RelativeItem> mRelatives;
    @ForeignCollectionField(columnName = "declarations")
    protected Collection<DeclarationItem> mDeclarations;

    DeputyItem() {
    }

    public DeputyItem(final int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public PartyItem getParty() {
        return mParty;
    }

    public void setParty(final PartyItem party) {
        mParty = party;
    }

    public RegionItem getRegion() {
        return mRegion;
    }

    public void setRegion(final RegionItem region) {
        mRegion = region;
    }

    public int getDistrict() {
        return mDistrict;
    }

    public void setDistrict(final int district) {
        mDistrict = district;
    }

    public int getConvocation() {
        return mConvocation;
    }

    public void setConvocation(final int convocation) {
        mConvocation = convocation;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(final String thumbnail) {
        mThumbnail = thumbnail;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(final String description) {
        mDescription = description;
    }

    public String[] getEmails() {
        return mEmails;
    }

    public void setEmails(final String[] emails) {
        mEmails = emails;
    }

    public String[] getPhones() {
        return mPhones;
    }

    public void setPhones(final String[] phones) {
        mPhones = phones;
    }

    public String[] getSocials() {
        return mSocials;
    }

    public void setSocials(final String[] socials) {
        mSocials = socials;
    }

    public Collection<AssistantItem> getAssistants() {
        return mAssistants;
    }

    public void setAssistants(final Collection<AssistantItem> assistants) {
        mAssistants = assistants;
    }

    public Collection<RelativeItem> getRelatives() {
        return mRelatives;
    }

    public void setRelatives(final Collection<RelativeItem> relatives) {
        mRelatives = relatives;
    }

    public Collection<DeclarationItem> getDeclarations() {
        return mDeclarations;
    }

    public void setDeclarations(final Collection<DeclarationItem> declarations) {
        mDeclarations = declarations;
    }
}
