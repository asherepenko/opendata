package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.sherepenko.opendata.core.data.AssistantTypeItem;

import java.sql.SQLException;

public class AssistantTypeDaoImpl extends BaseDaoImpl<AssistantTypeItem, Integer> implements AssistantTypeDao {

    public AssistantTypeDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, AssistantTypeItem.class);
        setObjectCache(ReferenceObjectCache.makeSoftCache());
    }
}
