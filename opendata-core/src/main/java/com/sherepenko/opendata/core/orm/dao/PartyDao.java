package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.Dao;
import com.sherepenko.opendata.core.data.PartyItem;

public interface PartyDao extends Dao<PartyItem, Integer> {
}