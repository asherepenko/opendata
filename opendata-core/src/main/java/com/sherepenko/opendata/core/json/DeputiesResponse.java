package com.sherepenko.opendata.core.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sherepenko.opendata.core.data.DeputyItem;
import com.sherepenko.opendata.core.json.deserialization.DeputiesResponseDeserializer;

import java.util.List;

@JsonDeserialize(using = DeputiesResponseDeserializer.class)
public class DeputiesResponse extends ServiceResponse<DeputyItem> {

    public DeputiesResponse(final List<DeputyItem> items) {
        super(items);
    }
}
