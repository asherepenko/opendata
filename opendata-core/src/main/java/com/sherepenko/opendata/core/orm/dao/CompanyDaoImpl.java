package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.sherepenko.opendata.core.data.CompanyItem;

import java.sql.SQLException;

public class CompanyDaoImpl extends BaseDaoImpl<CompanyItem, Integer> implements CompanyDao {

    public CompanyDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, CompanyItem.class);
        setObjectCache(ReferenceObjectCache.makeSoftCache());
    }
}
