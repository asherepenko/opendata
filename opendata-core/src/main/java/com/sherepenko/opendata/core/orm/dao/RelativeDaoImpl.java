package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.sherepenko.opendata.core.data.RelativeItem;

import java.sql.SQLException;

public class RelativeDaoImpl extends BaseDaoImpl<RelativeItem, Integer> implements RelativeDao {

    public RelativeDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, RelativeItem.class);
        setObjectCache(ReferenceObjectCache.makeSoftCache());
    }
}
