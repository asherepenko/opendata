package com.sherepenko.opendata.core.orm;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sherepenko.opendata.core.data.*;
import com.sherepenko.opendata.core.orm.dao.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

public class OrmHelper {

    private static final OrmHelper sInstance = new OrmHelper();

    private JdbcConnectionSource mConnectionSource;

    private AssistantDao mAssistantDao;
    private AssistantTypeDao mAssistantTypeDao;
    private CompanyDao mCompanyDao;
    private DeclarationDao mDeclarationDao;
    private DeputyDao mDeputyDao;
    private PartyDao mPartyDao;
    private RegionDao mRegionDao;
    private RelativeDao mRelativeDao;

    public static OrmHelper get() {
        return sInstance;
    }

    private OrmHelper() {
        System.setProperty("com.j256.ormlite.logger.level", "ERROR");

        try {
            mConnectionSource = new JdbcConnectionSource("jdbc:h2:file:./opendata");

            mAssistantDao = DaoManager.createDao(mConnectionSource, AssistantItem.class);
            mAssistantTypeDao = DaoManager.createDao(mConnectionSource, AssistantTypeItem.class);
            mCompanyDao = DaoManager.createDao(mConnectionSource, CompanyItem.class);
            mDeclarationDao = DaoManager.createDao(mConnectionSource, DeclarationItem.class);
            mDeputyDao = DaoManager.createDao(mConnectionSource, DeputyItem.class);
            mPartyDao = DaoManager.createDao(mConnectionSource, PartyItem.class);
            mRegionDao = DaoManager.createDao(mConnectionSource, RegionItem.class);
            mRelativeDao = DaoManager.createDao(mConnectionSource, RelativeItem.class);

            createTablesIfNotExist(mConnectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public AssistantDao getAssistantDao() {
        return mAssistantDao;
    }

    public AssistantTypeDao getAssistantTypeDao() {
        return mAssistantTypeDao;
    }

    public CompanyDao getCompanyDao() {
        return mCompanyDao;
    }

    public DeclarationDao getDeclarationDao() {
        return mDeclarationDao;
    }

    public DeputyDao getDeputyDao() {
        return mDeputyDao;
    }

    public PartyDao getPartyDao() {
        return mPartyDao;
    }

    public RegionDao getRegionDao() {
        return mRegionDao;
    }

    public RelativeDao getRelativeDao() {
        return mRelativeDao;
    }

    public void insertAssistant(final AssistantItem assistant) {
        try {
            mAssistantDao.createIfNotExists(assistant);
            assistant.setDao(mAssistantDao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertAssistantType(final AssistantTypeItem assistantType) {
        try {
            mAssistantTypeDao.createIfNotExists(assistantType);
            assistantType.setDao(mAssistantDao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertCompany(final CompanyItem company) {
        try {
            mCompanyDao.createIfNotExists(company);
            company.setDao(mCompanyDao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertDeclaration(final DeclarationItem declaration) {
        try {
            mDeclarationDao.createIfNotExists(declaration);
            declaration.setDao(mDeclarationDao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertDeputy(final DeputyItem deputy) {
        try {
            mDeputyDao.createIfNotExists(deputy);
            deputy.setDao(mDeputyDao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertParty(final PartyItem party) {
        try {
            mPartyDao.createIfNotExists(party);
            party.setDao(mPartyDao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertRegion(final RegionItem region) {
        try {
            mRegionDao.createIfNotExists(region);
            region.setDao(mRegionDao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertRelative(final RelativeItem relative) {
        try {
            mRelativeDao.createIfNotExists(relative);
            relative.setDao(mRelativeDao);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertAssistants(final List<AssistantItem> assistants) {
        try {
            mAssistantDao.callBatchTasks((Callable<Void>) () -> {
                assistants.forEach(this::insertAssistant);
                return null;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertAssistantTypes(final List<AssistantTypeItem> assistantTypes) {
        try {
            mAssistantTypeDao.callBatchTasks((Callable<Void>) () -> {
                assistantTypes.forEach(this::insertAssistantType);
                return null;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertCompanies(final List<CompanyItem> companies) {
        try {
            mCompanyDao.callBatchTasks((Callable<Void>) () -> {
                companies.forEach(this::insertCompany);
                return null;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertDeclarations(final List<DeclarationItem> declarations) {
        try {
            mDeclarationDao.callBatchTasks((Callable<Void>) () -> {
                declarations.forEach(this::insertDeclaration);
                return null;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertDeputies(final List<DeputyItem> deputies) {
        try {
            mDeputyDao.callBatchTasks((Callable<Void>) () -> {
                deputies.forEach(this::insertDeputy);
                return null;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertParties(final List<PartyItem> parties) {
        try {
            mPartyDao.callBatchTasks((Callable<Void>) () -> {
                parties.forEach(this::insertParty);
                return null;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertRegions(final List<RegionItem> regions) {
        try {
            mRegionDao.callBatchTasks((Callable<Void>) () -> {
                regions.forEach(this::insertRegion);
                return null;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertRelatives(final List<RelativeItem> relatives) {
        try {
            mRelativeDao.callBatchTasks((Callable<Void>) () -> {
                relatives.forEach(this::insertRelative);
                return null;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void shutdown() {
        try {
            if (mConnectionSource != null) {
                mConnectionSource.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createTablesIfNotExist(JdbcConnectionSource connectionSource) throws SQLException {
        TableUtils.createTableIfNotExists(connectionSource, AssistantItem.class);
        TableUtils.createTableIfNotExists(connectionSource, AssistantTypeItem.class);
        TableUtils.createTableIfNotExists(connectionSource, CompanyItem.class);
        TableUtils.createTableIfNotExists(connectionSource, DeclarationItem.class);
        TableUtils.createTableIfNotExists(connectionSource, DeputyItem.class);
        TableUtils.createTableIfNotExists(connectionSource, PartyItem.class);
        TableUtils.createTableIfNotExists(connectionSource, RegionItem.class);
        TableUtils.createTableIfNotExists(connectionSource, RelativeItem.class);
    }
}
