package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.sherepenko.opendata.core.data.DeclarationItem;

import java.sql.SQLException;

public class DeclarationDaoImpl extends BaseDaoImpl<DeclarationItem, Integer> implements DeclarationDao {

    public DeclarationDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DeclarationItem.class);
        setObjectCache(ReferenceObjectCache.makeSoftCache());
    }
}
