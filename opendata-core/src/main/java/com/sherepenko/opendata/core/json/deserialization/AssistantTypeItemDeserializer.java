package com.sherepenko.opendata.core.json.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.sherepenko.opendata.core.data.AssistantTypeItem;
import com.sherepenko.opendata.core.orm.OrmHelper;

import java.io.IOException;

public class AssistantTypeItemDeserializer extends JsonDeserializer<AssistantTypeItem> {

    @Override
    public AssistantTypeItem deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        final JsonNode jsonRoot = jsonParser.getCodec().readTree(jsonParser);
        final AssistantTypeItem assistantType = new AssistantTypeItem(
                jsonRoot.get("id").asInt(),
                jsonRoot.get("type").asText());
        OrmHelper.get().insertAssistantType(assistantType);
        return assistantType;
    }
}
