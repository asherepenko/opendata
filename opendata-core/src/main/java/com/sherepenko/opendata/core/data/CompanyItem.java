package com.sherepenko.opendata.core.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.sherepenko.opendata.core.orm.dao.CompanyDaoImpl;
import org.apache.commons.lang3.StringUtils;

@DatabaseTable(tableName = "companies", daoClass = CompanyDaoImpl.class)
public class CompanyItem extends BaseDaoEnabled {

    @DatabaseField(columnName = "id", id = true)
    protected String mId;
    @DatabaseField(columnName = "declaration_id", foreign = true, foreignAutoRefresh = true)
    protected DeclarationItem mDeclaration;
    @DatabaseField(columnName = "name", dataType = DataType.LONG_STRING)
    protected String mName;
    @DatabaseField(columnName = "legal_form")
    protected String mLegalForm;
    @DatabaseField(columnName = "property_type")
    protected String mPropertyType;
    @DatabaseField(columnName = "address", dataType = DataType.LONG_STRING)
    protected String mAddress;
    @DatabaseField(columnName = "phone")
    protected String mPhone;
    @DatabaseField(columnName = "fax")
    protected String mFax;

    public CompanyItem() {
    }

    public CompanyItem(final String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public DeclarationItem getDeclaration() {
        return mDeclaration;
    }

    public void setDeclaration(final DeclarationItem declaration) {
        mDeclaration = declaration;
    }

    public String getName() {
        return mName;
    }

    public void setName(final String name) {
        mName = name;
    }

    public String getLegalForm() {
        return mLegalForm;
    }

    public void setLegalForm(final String legalForm) {
        mLegalForm = legalForm;
    }

    public String getPropertyType() {
        return mPropertyType;
    }

    public void setPropertyType(final String propertyType) {
        mPropertyType = propertyType;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(final String address) {
        mAddress = address;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(final String phone) {
        mPhone = phone;
    }

    public String getFax() {
        return mFax;
    }

    public void setFax(final String fax) {
        mFax = fax;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (other == null) {
            return false;
        } else if (getClass() != other.getClass()) {
            return false;
        }

        CompanyItem item = ((CompanyItem) other);
        return StringUtils.equals(mId, item.mId);
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + (mId != null ? mId.hashCode() : 0);
        result = 31 * result + (mName != null ? mName.hashCode() : 0);
        return result;
    }
}
