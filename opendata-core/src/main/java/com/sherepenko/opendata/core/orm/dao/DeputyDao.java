package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.Dao;
import com.sherepenko.opendata.core.data.DeputyItem;

public interface DeputyDao extends Dao<DeputyItem, Integer> {
}
