package com.sherepenko.opendata.core.loaders;

import com.sherepenko.opendata.core.api.ParliamentService;
import com.sherepenko.opendata.core.api.ServiceFactory;
import com.sherepenko.opendata.core.data.DeputyItem;
import com.sherepenko.opendata.core.json.ServiceResponse;
import io.reactivex.Observable;

import java.util.List;

public class ParliamentServiceLoader {

    private final ParliamentService mService =
            ServiceFactory.createService(ParliamentService.class, ParliamentService.BASE_URL);

    public Observable<List<DeputyItem>> getDeputies() {
        return mService.getDeputies()
                .map(ServiceResponse::getItems)
                .doOnComplete(() -> System.out.println("GET request finished successfully"))
                .doOnError(throwable -> System.out.println("Error occurred: " + throwable.getMessage()));
    }
}
