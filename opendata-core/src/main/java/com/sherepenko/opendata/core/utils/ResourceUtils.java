package com.sherepenko.opendata.core.utils;

import java.io.File;
import java.net.URL;

public class ResourceUtils {

    private ResourceUtils() {
    }

    public static File getResource(final String path) {
        final URL resource = ResourceUtils.class.getClassLoader().getResource(path);
        return resource != null ? new File(resource.getFile()) : null;
    }
}
