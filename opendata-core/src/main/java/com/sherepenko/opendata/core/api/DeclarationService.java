package com.sherepenko.opendata.core.api;

import com.sherepenko.opendata.core.json.DeclarationsResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

import java.util.*;

public interface DeclarationService {

    String BASE_URL = "https://declarations.com.ua";

    @GET("search?format=opendata")
    default Observable<DeclarationsResponse> getDeclarations(@Query("q") String query) {
        final List<String> sections = new ArrayList<>();
        sections.add("infocard");
        sections.add("unified_source");
        return getDeclarations(query, sections);
    }

    @GET("search?format=opendata")
    default Observable<DeclarationsResponse> getDeclarations(@Query("q") String query, @Query("section") List<String> sections) {
        final Map<String, String> params = new HashMap<>();
        params.put("doc_type", "Щорічна");
        return getDeclarations(query, sections, params);
    }

    @GET("search?format=opendata")
    Observable<DeclarationsResponse> getDeclarations(@Query("q") String query, @Query("section") List<String> sections, @QueryMap Map<String, String> params);
}
