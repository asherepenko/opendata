package com.sherepenko.opendata.core.loaders;

import com.sherepenko.opendata.core.api.DeclarationService;
import com.sherepenko.opendata.core.api.ServiceFactory;
import com.sherepenko.opendata.core.data.DeclarationItem;
import com.sherepenko.opendata.core.json.ServiceResponse;
import io.reactivex.Observable;

import java.util.List;

public class DeclarationServiceLoader {

    private final DeclarationService mService =
            ServiceFactory.createService(DeclarationService.class, DeclarationService.BASE_URL);

    public Observable<List<DeclarationItem>> getDeclarations(String fullName) {
        return mService.getDeclarations(fullName)
                .map(ServiceResponse::getItems)
                .doOnComplete(() -> System.out.println("GET request finished successfully"))
                .doOnError(throwable -> System.out.println("Error occurred: " + throwable.getMessage()));
    }
}
