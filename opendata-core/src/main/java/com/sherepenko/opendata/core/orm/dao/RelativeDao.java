package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.Dao;
import com.sherepenko.opendata.core.data.RelativeItem;

public interface RelativeDao extends Dao<RelativeItem, Integer> {
}