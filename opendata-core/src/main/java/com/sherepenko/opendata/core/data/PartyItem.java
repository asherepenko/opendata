package com.sherepenko.opendata.core.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.sherepenko.opendata.core.json.deserialization.PartyItemDeserializer;
import com.sherepenko.opendata.core.orm.dao.PartyDaoImpl;

@DatabaseTable(tableName = "parties", daoClass = PartyDaoImpl.class)
@JsonDeserialize(using = PartyItemDeserializer.class)
public class PartyItem extends BaseDaoEnabled {

    @DatabaseField(columnName = "id", id = true)
    protected int mId;
    @DatabaseField(columnName = "name", dataType = DataType.LONG_STRING)
    protected String mName;

    PartyItem() {
    }

    @JsonCreator
    public PartyItem(@JsonProperty("id") final int id,
                     @JsonProperty("name") final String name) {
        mId = id;
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (other == null) {
            return false;
        } else if (getClass() != other.getClass()) {
            return false;
        }

        PartyItem item = ((PartyItem) other);
        return mId == item.mId;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + mId;
        result = 31 * result + (mName != null ? mName.hashCode() : 0);
        return result;
    }
}
