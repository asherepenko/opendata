package com.sherepenko.opendata.core.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.sherepenko.opendata.core.orm.dao.DeclarationDaoImpl;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Date;

@DatabaseTable(tableName = "declarations", daoClass = DeclarationDaoImpl.class)
public class DeclarationItem extends BaseDaoEnabled {

    @DatabaseField(columnName = "id", id = true)
    protected String mId;
    @DatabaseField(columnName = "mp_id", foreign = true, foreignAutoRefresh = true)
    protected DeputyItem mDeputy;
    @DatabaseField(columnName = "year")
    protected int mYear;
    @DatabaseField(columnName = "position", dataType = DataType.LONG_STRING)
    protected String mPosition;
    @DatabaseField(columnName = "office")
    protected String mOffice;
    @DatabaseField(columnName = "url")
    protected String mUrl;
    @DatabaseField(columnName = "created_date", dataType = DataType.DATE_LONG)
    protected Date mCreatedDate;
    @ForeignCollectionField(columnName = "relatives")
    protected Collection<RelativeItem> mRelatives;
    @ForeignCollectionField(columnName = "companies")
    protected Collection<CompanyItem> mCompanies;

    DeclarationItem() {
    }

    public DeclarationItem(final String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public DeputyItem getDeputy() {
        return mDeputy;
    }

    public void setDeputy(final DeputyItem deputy) {
        mDeputy = deputy;
    }

    public int getYear() {
        return mYear;
    }

    public void setYear(final int year) {
        mYear = year;
    }

    public String getPosition() {
        return mPosition;
    }

    public void setPosition(final String position) {
        mPosition = position;
    }

    public String getOffice() {
        return mOffice;
    }

    public void setOffice(final String office) {
        mOffice = office;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(final String url) {
        mUrl = url;
    }

    public Date getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(final Date createdDate) {
        mCreatedDate = createdDate;
    }

    public Collection<RelativeItem> getRelatives() {
        return mRelatives;
    }

    public void setRelatives(final Collection<RelativeItem> relatives) {
        mRelatives = relatives;
    }

    public Collection<CompanyItem> getCompanies() {
        return mCompanies;
    }

    public void setCompanies(final Collection<CompanyItem> companies) {
        mCompanies = companies;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (other == null) {
            return false;
        } else if (getClass() != other.getClass()) {
            return false;
        }

        DeclarationItem item = ((DeclarationItem) other);
        return StringUtils.equals(mId, item.mId);
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + mYear;
        result = 31 * result + (mId != null ? mId.hashCode() : 0);
        result = 31 * result + (mUrl != null ? mUrl.hashCode() : 0);
        return result;
    }
}
