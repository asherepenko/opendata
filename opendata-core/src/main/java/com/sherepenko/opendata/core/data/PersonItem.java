package com.sherepenko.opendata.core.data;


import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.Objects;

public abstract class PersonItem extends BaseDaoEnabled implements Comparable<PersonItem> {

    @DatabaseField(columnName = "last_name")
    protected String mLastName;
    @DatabaseField(columnName = "first_name")
    protected String mFirstName;
    @DatabaseField(columnName = "patronymic")
    protected String mPatronymic;
    @DatabaseField(columnName = "birth_date", dataType = DataType.DATE_LONG)
    protected Date mBirthDate;

    PersonItem() {
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(final String lastName) {
        mLastName = lastName;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(final String firstName) {
        mFirstName = firstName;
    }

    public String getPatronymic() {
        return mPatronymic;
    }

    public void setPatronymic(final String patronymic) {
        mPatronymic = patronymic;
    }

    public String getFullName() {
        return StringUtils.joinWith(" ", mLastName, mFirstName, mPatronymic);
    }

    public Date getBirthDate() {
        return mBirthDate;
    }

    public void setBirthDate(final Date birthDate) {
        mBirthDate = birthDate;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (other == null) {
            return false;
        } else if (getClass() != other.getClass()) {
            return false;
        }

        PersonItem item = ((PersonItem) other);
        return StringUtils.equalsIgnoreCase(mLastName, item.mLastName) &&
                StringUtils.equalsIgnoreCase(mFirstName, item.mFirstName) &&
                StringUtils.equalsIgnoreCase(mPatronymic, item.mPatronymic) &&
                Objects.equals(mBirthDate, item.mBirthDate);
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = 31 * hash + (mLastName != null ? mLastName.hashCode() : 0);
        hash = 31 * hash + (mFirstName != null ? mFirstName.hashCode() : 0);
        hash = 31 * hash + (mPatronymic != null ? mPatronymic.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(PersonItem other) {
        int result = 0;
        if (equals(other)) {
            return result;
        }

        result = StringUtils.compareIgnoreCase(mLastName, other.mLastName);
        if (result != 0) {
            return result;
        }

        result = StringUtils.compareIgnoreCase(mFirstName, other.mFirstName);
        if (result != 0) {
            return result;
        }

        result = StringUtils.compareIgnoreCase(mPatronymic, other.mPatronymic);
        if (result != 0) {
            return result;
        }

        return ObjectUtils.compare(mBirthDate, other.mBirthDate);
    }
}
