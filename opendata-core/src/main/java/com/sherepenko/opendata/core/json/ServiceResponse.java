package com.sherepenko.opendata.core.json;

import java.util.List;

public abstract class ServiceResponse<DI> {

    private final List<DI> mItems;

    ServiceResponse(final List<DI> items) {
        mItems = items;
    }

    public List<DI> getItems() {
        return mItems;
    }

    public int getItemsCount() {
        return mItems != null ? mItems.size() : 0;
    }
}
