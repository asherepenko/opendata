package com.sherepenko.opendata.core.json.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.sherepenko.opendata.core.data.*;
import com.sherepenko.opendata.core.json.DeputiesResponse;
import com.sherepenko.opendata.core.orm.OrmHelper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DeputiesResponseDeserializer extends JsonDeserializer<DeputiesResponse> {

    private static final DateFormat sFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Override
    public DeputiesResponse deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        final JsonNode jsonRoot = jsonParser.getCodec().readTree(jsonParser);
        final List<DeputyItem> deputies =
                StreamSupport.stream(jsonRoot.get("mps").spliterator(), false)
                        .map(this::createDeputyItem)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList());
        return new DeputiesResponse(deputies);
    }

    private DeputyItem createDeputyItem(final JsonNode jsonDeputy) {
        final DeputyItem deputy = new DeputyItem(jsonDeputy.get("id").asInt());
        if (jsonDeputy.get("party_id").isInt()) {
            setParty(deputy, jsonDeputy.get("party_id").asInt());
        }
        if (jsonDeputy.get("region_id").isInt()) {
            setRegion(deputy, jsonDeputy.get("region_id").asInt());
        }
        if (jsonDeputy.get("district_num").isInt()) {
            deputy.setDistrict(jsonDeputy.get("district_num").asInt());
        }
        if (jsonDeputy.get("convocation").isInt()) {
            deputy.setConvocation(jsonDeputy.get("convocation").asInt());
        }
        if (jsonDeputy.get("surname").isTextual()) {
            deputy.setLastName(jsonDeputy.get("surname").asText());
        }
        if (jsonDeputy.get("firstname").isTextual()) {
            deputy.setFirstName(jsonDeputy.get("firstname").asText());
        }
        if (jsonDeputy.get("patronymic").isTextual()) {
            deputy.setPatronymic(jsonDeputy.get("patronymic").asText());
        }
        if (jsonDeputy.get("birthday").isTextual()) {
            deputy.setBirthDate(parseDate(jsonDeputy.get("birthday").asText()));
        }
        if (jsonDeputy.get("photo").isTextual()) {
            deputy.setThumbnail(jsonDeputy.get("photo").asText());
        }
        if (jsonDeputy.get("short_info").isTextual()) {
            deputy.setDescription(jsonDeputy.get("short_info").asText());
        }
        if (jsonDeputy.get("emails").isArray()) {
            deputy.setEmails(
                    StreamSupport.stream(jsonDeputy.get("emails").spliterator(), false)
                            .filter(jsonEmail -> jsonEmail.get("value").isTextual())
                            .map(jsonEmail -> jsonEmail.get("value").asText().toLowerCase())
                            .toArray(String[]::new));
        }
        if (jsonDeputy.get("phones").isArray()) {
            deputy.setPhones(
                    StreamSupport.stream(jsonDeputy.get("phones").spliterator(), false)
                            .filter(jsonPhone -> jsonPhone.get("value").isTextual())
                            .map(jsonPhone -> jsonPhone.get("value").asText().toLowerCase())
                            .toArray(String[]::new));
        }
        if (jsonDeputy.get("socials").isArray()) {
            deputy.setSocials(
                    StreamSupport.stream(jsonDeputy.get("socials").spliterator(), false)
                            .filter(jsonSocial -> jsonSocial.get("url").isTextual())
                            .map(jsonSocial -> jsonSocial.get("url").asText().toLowerCase())
                            .toArray(String[]::new));
        }
        if (jsonDeputy.get("assistants").isArray()) {
            deputy.setAssistants(
                    StreamSupport.stream(jsonDeputy.get("assistants").spliterator(), false)
                            .map(jsonAssistant -> createAssistantItem(deputy, jsonAssistant))
                            .distinct()
                            .sorted()
                            .collect(Collectors.toList()));
        }
        if (jsonDeputy.get("declarations").isArray()) {
            deputy.setRelatives(
                    StreamSupport.stream(jsonDeputy.get("declarations").spliterator(), false)
                            .filter(jsonDeclaration -> jsonDeclaration.get("family_members").isArray())
                            .flatMap(jsonDeclaration ->
                                    StreamSupport.stream(
                                            jsonDeclaration.get("family_members").spliterator(), false))
                            .map(jsonRelative -> createRelativeItem(deputy, jsonRelative))
                            .distinct()
                            .sorted()
                            .collect(Collectors.toList()));
        }
        OrmHelper.get().insertDeputy(deputy);
        return deputy;
    }

    private AssistantItem createAssistantItem(final DeputyItem deputy, final JsonNode jsonAssistant) {
        final AssistantItem assistant = new AssistantItem();
        assistant.setDeputy(deputy);
        if (jsonAssistant.get("type_id").isInt()) {
            setAssistantType(assistant, jsonAssistant.get("type_id").asInt());
        }
        if (jsonAssistant.get("fullname").isTextual()) {
            setupNames(assistant, jsonAssistant.get("fullname").asText());
        }
        OrmHelper.get().insertAssistant(assistant);
        return assistant;
    }

    private RelativeItem createRelativeItem(final DeputyItem deputy, final JsonNode jsonRelative) {
        final RelativeItem relative = new RelativeItem();
        relative.setDeputy(deputy);
        if (jsonRelative.get("relationship").isTextual()) {
            relative.setRelationship(jsonRelative.get("relationship").asText().toLowerCase());
        }
        if (jsonRelative.get("fullname").isTextual()) {
            setupNames(relative, jsonRelative.get("fullname").asText());
        }
        OrmHelper.get().insertRelative(relative);
        return relative;
    }

    private static Date parseDate(final String date) {
        try {
            return sFormatter.parse(date);
        } catch (ParseException ignore) {}

        return null;
    }

    private static void setupNames(final PersonItem person, final String fullName) {
        final String[] names = fullName.replaceAll("(?<=\\.)(?!$)", " ").split("\\s+");

        if (names.length == 1) {
            person.setLastName(names[0]);
        } else if (names.length == 2) {
            person.setLastName(names[0]);
            person.setFirstName(names[1]);
        } else if (names.length >= 3) {
            person.setLastName(names[0]);
            person.setFirstName(names[1]);
            person.setPatronymic(StringUtils.join(Arrays.copyOfRange(names, 2, names.length), " "));
        }
    }

    private static void setParty(final DeputyItem deputy, final int partyId) {
        try {
            deputy.setParty(OrmHelper.get().getPartyDao().queryForId(partyId));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void setRegion(final DeputyItem deputy, final int regionId) {
        try {
            deputy.setRegion(OrmHelper.get().getRegionDao().queryForId(regionId));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void setAssistantType(final AssistantItem assistant, final int typeId) {
        try {
            assistant.setAssistantType(OrmHelper.get().getAssistantTypeDao().queryForId(typeId));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
