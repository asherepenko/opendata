package com.sherepenko.opendata.core.orm.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.sherepenko.opendata.core.data.RegionItem;

import java.sql.SQLException;

public class RegionDaoImpl extends BaseDaoImpl<RegionItem, Integer> implements RegionDao {

    public RegionDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, RegionItem.class);
        setObjectCache(ReferenceObjectCache.makeSoftCache());
    }
}
