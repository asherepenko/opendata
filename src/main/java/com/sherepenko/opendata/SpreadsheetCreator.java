package com.sherepenko.opendata;

import com.sherepenko.opendata.core.data.*;
import com.sherepenko.opendata.core.loaders.DeclarationServiceLoader;
import com.sherepenko.opendata.core.loaders.ParliamentServiceLoader;
import io.reactivex.Observable;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class SpreadsheetCreator {

    private static final String ENUM_PATTERN = "01-%05d";

    private static final DateFormat sFormatter = new SimpleDateFormat("dd.MM.yyyy");

    private final AtomicInteger mDeputiesCount = new AtomicInteger(1);
    private final AtomicInteger mAssistantsCount = new AtomicInteger(1);
    private final AtomicInteger mRelativesCount = new AtomicInteger(1);
    private final AtomicInteger mDeclarationsCount = new AtomicInteger(1);
    private final AtomicInteger mCompaniesCount = new AtomicInteger(1);

    private final ParliamentServiceLoader mParliamentServiceLoader = new ParliamentServiceLoader();
    private final DeclarationServiceLoader mDeclarationServiceLoader = new DeclarationServiceLoader();

    private final SpreadsheetHelper mSpreadsheetHelper = new SpreadsheetHelper();

    private final String mFileName;

    public SpreadsheetCreator(final String fileName) {
        mFileName = fileName;
        init();
    }

    private void init() {
        mParliamentServiceLoader.getDeputies()
                .doOnNext(deputies -> System.out.println("Deputies = " + deputies.size()))
                .flatMap(Observable::fromIterable)
                .doOnNext(this::createPartnerRows)
                .doOnNext(this::createRelativeRows)
                .doOnNext(this::createDeputyRow)
                .doOnNext(this::createDeclarationRows)
                .doOnNext(this::createCompanyRows)
                .doOnNext(deputy -> mDeputiesCount.addAndGet(1))
                .doOnComplete(() -> mSpreadsheetHelper.writeTo(mFileName))
                .subscribe();
    }

    private void createPartnerRows(final DeputyItem deputy) {
        deputy.getAssistants()
                .forEach(assistant ->
                        createPartnerRow(deputy, assistant));
    }

    private void createRelativeRows(final DeputyItem deputy) {
        deputy.getRelatives()
                .forEach(relative ->
                        createRelativeRow(deputy, relative));
    }

    private void createDeclarationRows(final DeputyItem deputy) {
        mDeclarationServiceLoader.getDeclarations(deputy.getFullName())
                .flatMap(Observable::fromIterable)
                .forEach(declaration ->
                        createDeclarationRow(deputy, declaration));
    }

    private void createCompanyRows(final DeputyItem deputy) {
        mDeclarationServiceLoader.getDeclarations(deputy.getFullName())
                .flatMap(Observable::fromIterable)
                .map(DeclarationItem::getCompanies)
                .flatMap(Observable::fromIterable)
                .forEach(company ->
                        createCompanyRow(deputy, company));
    }

    private void createDeputyRow(final DeputyItem deputy) {
        final HSSFRow row = mSpreadsheetHelper.getDeputiesSheet().createRow(mDeputiesCount.get());
        row.createCell(0).setCellValue(String.format(ENUM_PATTERN, mDeputiesCount.get()));
        row.createCell(1).setCellValue(deputy.getLastName());
        row.createCell(2).setCellValue(
                StringUtils.joinWith(" ", deputy.getFirstName(), deputy.getPatronymic()));
        row.createCell(3).setCellValue(deputy.getThumbnail());
        row.createCell(4).setCellValue(
                sFormatter.format(deputy.getBirthDate()));
        if (deputy.getParty() != null) {
            row.createCell(6).setCellValue(deputy.getParty().getName());
        }
        if (deputy.getRegion() != null) {
            row.createCell(11).setCellValue(deputy.getRegion().getName());
        }
        row.createCell(14).setCellValue(sFormatter.format(new Date()));
    }

    private void createPartnerRow(final DeputyItem deputy, final AssistantItem assistant) {
        final HSSFRow row = mSpreadsheetHelper.getPartnersSheet().createRow(mAssistantsCount.get());
        row.createCell(0).setCellValue(String.format(ENUM_PATTERN, mDeputiesCount.get()));
        row.createCell(1).setCellValue(deputy.getFullName());
        row.createCell(3).setCellValue(assistant.getLastName());
        row.createCell(4).setCellValue(
                StringUtils.joinWith(" ", assistant.getFirstName(), assistant.getPatronymic()));
        if (assistant.getAssistantType() != null) {
            row.createCell(5).setCellValue(assistant.getAssistantType().getType());
        }
        row.createCell(15).setCellValue(sFormatter.format(new Date()));
        mAssistantsCount.addAndGet(1);
    }

    private void createRelativeRow(final DeputyItem deputy, final RelativeItem relative) {
        final HSSFRow row = mSpreadsheetHelper.getRelativesSheet().createRow(mRelativesCount.get());
        row.createCell(0).setCellValue(String.format(ENUM_PATTERN, mDeputiesCount.get()));
        row.createCell(1).setCellValue(deputy.getFullName());
        row.createCell(3).setCellValue(relative.getLastName());
        row.createCell(4).setCellValue(
                StringUtils.joinWith(" ", relative.getFirstName(), relative.getPatronymic()));
        row.createCell(5).setCellValue(relative.getRelationship());
        row.createCell(15).setCellValue(sFormatter.format(new Date()));
        mRelativesCount.addAndGet(1);
    }

    private void createDeclarationRow(final DeputyItem deputy, final DeclarationItem declaration) {
        final HSSFRow row = mSpreadsheetHelper.getDeclarationsSheet().createRow(mDeclarationsCount.get());
        row.createCell(0).setCellValue(String.format(ENUM_PATTERN, mDeputiesCount.get()));
        row.createCell(1).setCellValue(deputy.getFullName());
        row.createCell(2).setCellValue(declaration.getYear());
        row.createCell(3).setCellValue(declaration.getUrl());
        row.createCell(5).setCellValue(sFormatter.format(new Date()));
        mDeclarationsCount.addAndGet(1);
    }

    private void createCompanyRow(final DeputyItem deputy, final CompanyItem company) {
        final HSSFRow row = mSpreadsheetHelper.getCompaniesSheet().createRow(mCompaniesCount.get());
        row.createCell(0).setCellValue(String.format(ENUM_PATTERN, mDeputiesCount.get()));
        row.createCell(1).setCellValue(deputy.getFullName());
        row.createCell(2).setCellValue(company.getId());
        row.createCell(3).setCellValue(company.getName());
        if (company.getLegalForm() != null) {
            row.createCell(4).setCellValue(company.getLegalForm());
        }
        if (company.getAddress() != null) {
            row.createCell(6).setCellValue(company.getAddress());
        }
        if (company.getPhone() != null) {
            row.createCell(7).setCellValue(company.getPhone());
        }
        row.createCell(10).setCellValue(company.getDeclaration().getUrl());
        row.createCell(12).setCellValue(sFormatter.format(new Date()));
        mCompaniesCount.addAndGet(1);
    }
}
