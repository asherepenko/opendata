package com.sherepenko.opendata;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.*;

public class SpreadsheetHelper {

    private static final String DEPUTIES_SHEET = "Депутати";
    private static final String RELATIVES_SHEET = "Родинні зв'язки";
    private static final String DECLARATIONS_SHEET = "Декларації";
    private static final String PARTNERS_SHEET = "Ділові партнери";
    private static final String COMPANIES_SHEET = "Компанії";
    private static final String FOUNDERS_SHEET = "Засновники компаній";

    private static final String[] DEPUTY_COLUMNS = new String[] {
            "Внутрішній ідентифікатор",             // 0
            "Прізвище",                             // 1
            "Ім'я, по-батькові",                    // 2
            "Фото",                                 // 3
            "Дата народження",                      // 4
            "Посада",                               // 5
            "Фракційна / партійна приналежність",   // 6
            "Сфера інтересів",                      // 7
            "Теги",                                 // 8
            "Адреса",                               // 9
            "Ідентифікаційний код",                 // 10
            "Місто",                                // 11
            "Орган влади",                          // 12
            "Примітки",                             // 13
            "Оновлено"                              // 14
    };

    private static final String[] RELATIVE_COLUMNS = new String[] {
            "Внутрішній ідентифікатор депутата",    // 0
            "ПІБ депутата",                         // 1
            "",                                     // 2
            "Прізвище",                             // 3
            "Ім'я, по-батькові",                    // 4
            "Ступінь спорідненості",                // 5
            "Дата народження",                      // 6
            "Діяльність",                           // 7
            "Доказ спорідненості",                  // 8
            "Посилання",                            // 9
            "Адреса",                               // 10
            "Пов'язані компанії, назва",            // 11
            "Пов'язані компанії, ЄДРПОУ",           // 12
            "Статус в компанії",                    // 13
            "Примітки",                             // 14
            "Оновлено"                              // 15
    };

    private static final String[] DECLARATION_COLUMNS = new String[] {
            "Внутрішній ідентифікатор депутата",    // 0
            "ПІБ депутата",                         // 1
            "Рік декларування",                     // 2
            "Посилання",                            // 3
            "Примітки",                             // 4
            "Оновлено"                              // 5
    };

    private static final String[] PARTNER_COLUMNS = new String[] {
            "Внутрішній ідентифікатор депутата",    // 0
            "ПІБ депутата",                         // 1
            "",                                     // 2
            "Прізвище",                             // 3
            "Ім'я, по-батькові",                    // 4
            "Ступінь спорідненості",                // 5
            "Дата народження",                      // 6
            "Діяльність",                           // 7
            "Доказ спорідненості",                  // 8
            "Посилання",                            // 9
            "Адреса",                               // 10
            "Пов'язані компанії, назва",            // 11
            "Пов'язані компанії, ЄДРПОУ",           // 12
            "Статус в компанії",                    // 13
            "Примітки",                             // 14
            "Оновлено"                              // 15
    };

    private static final String[] COMPANY_COLUMNS = new String[] {
            "Внутрішній ідентифікатор депутата",    // 0
            "ПІБ депутата",                         // 1
            "ЄДРПОУ",                               // 2
            "Назва компанії",                       // 3
            "Організаційно-правова форма",          // 4
            "Дата створення",                       // 5
            "Адреса",                               // 6
            "Телефон",                              // 7
            "Основний вид діяльності",              // 8
            "Інші види діяльності",                 // 9
            "Посилання",                            // 10
            "Примітки",                             // 11
            "Оновлено"                              // 12
    };

    private static final String[] FOUNDER_COLUMNS = new String[] {
            "ЄДРПОУ",                               // 0
            "Назва компанії",                       // 1
            "",                                     // 2
            "Прізвище",                             // 3
            "Ім'я, по-батькові",                    // 4
            "Статус в компанії",                    // 5
            "Посилання",                            // 6
            "Примітки",                             // 7
            "Оновлено"                              // 8
    };

    private final HSSFWorkbook mWorkbook;
    private final HSSFSheet mDeputiesSheet;
    private final HSSFSheet mRelativesSheet;
    private final HSSFSheet mDeclarationsSheet;
    private final HSSFSheet mPartnersSheet;
    private final HSSFSheet mCompaniesSheet;
    private final HSSFSheet mFoundersSheet;

    public SpreadsheetHelper() {
        mWorkbook = new HSSFWorkbook();
        mDeputiesSheet = mWorkbook.createSheet(DEPUTIES_SHEET);
        mRelativesSheet = mWorkbook.createSheet(RELATIVES_SHEET);
        mDeclarationsSheet = mWorkbook.createSheet(DECLARATIONS_SHEET);
        mPartnersSheet = mWorkbook.createSheet(PARTNERS_SHEET);
        mCompaniesSheet = mWorkbook.createSheet(COMPANIES_SHEET);
        mFoundersSheet = mWorkbook.createSheet(FOUNDERS_SHEET);

        init();
    }

    public HSSFSheet getDeputiesSheet() {
        return mDeputiesSheet;
    }

    public HSSFSheet getRelativesSheet() {
        return mRelativesSheet;
    }

    public HSSFSheet getDeclarationsSheet() {
        return mDeclarationsSheet;
    }

    public HSSFSheet getPartnersSheet() {
        return mPartnersSheet;
    }

    public HSSFSheet getCompaniesSheet() {
        return mCompaniesSheet;
    }

    public HSSFSheet getFoundersSheet() {
        return mFoundersSheet;
    }

    public boolean writeTo(final String fileName) {
        try (OutputStream os = new BufferedOutputStream(new FileOutputStream(fileName))) {
            mWorkbook.write(os);
            return true;
        } catch (IOException ignore) {}

        return false;
    }

    private void init() {
        initDeputiesSheet();
        initRelativesSheet();
        initDeclarationsSheet();
        initPartnersSheet();
        initCompaniesSheet();
        initFoundersSheet();
    }

    private void initDeputiesSheet() {
        final HSSFRow headRow = mDeputiesSheet.createRow(0);
        for (int i = 0; i < DEPUTY_COLUMNS.length; i++) {
            headRow.createCell(i).setCellValue(DEPUTY_COLUMNS[i]);
        }
        mDeputiesSheet.createFreezePane(0, 1);
    }

    private void initRelativesSheet() {
        final HSSFRow headRow = mRelativesSheet.createRow(0);
        for (int i = 0; i < RELATIVE_COLUMNS.length; i++) {
            headRow.createCell(i).setCellValue(RELATIVE_COLUMNS[i]);
        }
        mRelativesSheet.createFreezePane(0, 1);
    }

    private void initDeclarationsSheet() {
        final HSSFRow headRow = mDeclarationsSheet.createRow(0);
        for (int i = 0; i < DECLARATION_COLUMNS.length; i++) {
            headRow.createCell(i).setCellValue(DECLARATION_COLUMNS[i]);
        }
        mDeclarationsSheet.createFreezePane(0, 1);
    }

    private void initPartnersSheet() {
        final HSSFRow headRow = mPartnersSheet.createRow(0);
        for (int i = 0; i < PARTNER_COLUMNS.length; i++) {
            headRow.createCell(i).setCellValue(PARTNER_COLUMNS[i]);
        }
        mPartnersSheet.createFreezePane(0, 1);
    }

    private void initCompaniesSheet() {
        final HSSFRow headRow = mCompaniesSheet.createRow(0);
        for (int i = 0; i < COMPANY_COLUMNS.length; i++) {
            headRow.createCell(i).setCellValue(COMPANY_COLUMNS[i]);
        }
        mCompaniesSheet.createFreezePane(0, 1);
    }

    private void initFoundersSheet() {
        final HSSFRow headRow = mFoundersSheet.createRow(0);
        for (int i = 0; i < FOUNDER_COLUMNS.length; i++) {
            headRow.createCell(i).setCellValue(FOUNDER_COLUMNS[i]);
        }
        mFoundersSheet.createFreezePane(0, 1);
    }
}
