package com.sherepenko.opendata;

import com.sherepenko.opendata.core.loaders.InitialLoader;
import com.sherepenko.opendata.core.orm.OrmHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    private static final DateFormat sFormatter = new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss");

    public static void main(String... args) {
        InitialLoader.setup();
        new SpreadsheetCreator("mps_" + sFormatter.format(new Date()) + ".xls");
        OrmHelper.get().shutdown();
    }
}